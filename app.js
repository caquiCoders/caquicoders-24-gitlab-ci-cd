const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

app.set('view engine', 'ejs');
app.set('views', './app/views');

app.use('/', express.static('app/public'));
app.use('/', require('./app/routes/index'));

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);  
});
